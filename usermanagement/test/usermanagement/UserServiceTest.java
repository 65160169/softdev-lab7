/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package usermanagement;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Asus
 */
public class UserServiceTest {
    UserService userService;
    public UserServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        userService = new UserService();
        User newadmin = new User("Admin","Administator","pass@1234",'M','A');
        User newuser1 = new User("User1","user1","pass@1234",'M','U');
        User newuser2 = new User("User2","user2","pass@1234",'M','U');
        User newuser3 = new User("User3","user3","pass@1234",'M','U');
        User newuser4 = new User("User4","user4","pass@1234",'M','U');
        userService.addUser(newadmin);
        userService.addUser(newuser1);
        userService.addUser(newuser2);
        userService.addUser(newuser3);
        userService.addUser(newuser4);
        
        
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of add method, of class UserService.
     */
    @Test
    public void testAddUser() {
        System.out.println("add");
        User newUser = new User("admin","Administator","pass@1234",'M','U');
        UserService instance = new UserService();
        User expResult = newUser;
        User result = instance.addUser(newUser);
        assertEquals(expResult, result);
        assertEquals(1, result.getId());

    }

    /**
     * Test of getuser method, of class UserService.
     */
    @Test
    public void testGetuser() {
        System.out.println("getuser");
        int index = 0;
        UserService instance = new UserService();
        User expResult = null;
        User result = instance.getuser(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getusers method, of class UserService.
     */
    @Test
    public void testGetusers() {
        System.out.println("getusers");
        UserService instance = new UserService();
        ArrayList<User> expResult = null;
        ArrayList<User> result = instance.getusers();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getsize method, of class UserService.
     */
    @Test
    public void testGetsize() {
        System.out.println("getsize");
        UserService instance = new UserService();
        int expResult = 0;
        int result = instance.getsize();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
